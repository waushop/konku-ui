# Konku UI

Konku UI is a user inferface built with React.js for Konku storage management web application.

## Installation

Clone this repository to your projects directory

```bash
git clone https://waushop@bitbucket.org/waushop/konku-ui.git
```

Switch to your repository's directory

```bash
cd konku-ui
```

Install dependencies

```bash
yarn install
```

## Usage

Start the app in the dev environment

```bash
yarn start
```

Start the app for production

```bash
yarn build
```
### Notes
- When both UI and API are running in your machine go to http://localhost:8080 and register a new user. All users registered will be normal users. To get admin user you have to manually change role value User to Admin in users database table (You can use pgAdmin for example). Initial admin user seeding is still in development.
- When creating new products you have to fill on Storage Unit field with it's ID that you get when creating Storage Units. Select box to make that proccess easier and faster is in develoment.

## Technologies used
- React.js
- Bootstrap
- Axios
- React-Table

## License
[MIT](https://choosealicense.com/licenses/mit/)