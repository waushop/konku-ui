import http from '../http-common';
import authHeader from './auth-header';

class UserService {
  getAll = () => {
    return http.get("/users", { headers: authHeader() });
  };

  get = id => {
    return http.get(`/users/${id}`, { headers: authHeader() });
  };
}

export default new UserService();
