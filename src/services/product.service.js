import http from "../http-common";
import authHeader from './auth-header';

const getAll = () => {
return http.get("/products", { headers: authHeader() });
};

const getUserProducts = () => {
  return http.get("/products/userproducts", { headers: authHeader() });
};

const get = id => {
  return http.get(`/products/${id}`, { headers: authHeader() });
};

const create = data => {
  return http.post("/products", data, { headers: authHeader() });
};

const update = (id, data) => {
  return http.put(`/products/${id}`, data, { headers: authHeader() });
};

const remove = id => {
  return http.delete(`/products/${id}`, { headers: authHeader() });
};

// eslint-disable-next-line
export default {
  getAll,
  getUserProducts,
  get,
  create,
  update,
  remove,
};