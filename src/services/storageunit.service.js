import http from "../http-common";
import authHeader from './auth-header';

const getAll = () => {
  return http.get("/storageunits", { headers: authHeader() });
};

const getUserStorageUnits = () => {
  return http.get("/storageunits/userstorageunits", { headers: authHeader() });
};

const get = id => {
  return http.get(`/storageunits/${id}`, { headers: authHeader() });
};

const create = data => {
  return http.post("/storageunits", data, { headers: authHeader() });
};

const update = (id, data) => {
  return http.put(`/storageunits/${id}`, data, { headers: authHeader() });
};

const remove = id => {
  return http.delete(`/storageunits/${id}`, { headers: authHeader() });
};

// eslint-disable-next-line
export default {
  getAll,
  getUserStorageUnits,
  get,
  create,
  update,
  remove,
};