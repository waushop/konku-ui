import React, { Component } from "react";
import { Switch, Route, Link } from "react-router-dom";
import "./App.css";

import { Role } from "./helpers/role";
import AuthService from "./services/auth.service";
import { AdminRoute } from "./components/users/adminroute";

import Login from "./components/users/login.component";
import Register from "./components/users/register.component";
import Profile from "./components/users/profile.component";

import Statistics from "./components/statistics/statistics";

import StorageUnits from "./components/storageunits/storageunits";
import AddStorageUnit from "./components/storageunits/add-storageunit";

import Products from "./components/products/products";
import AddProduct from "./components/products/add-product";
import EditProduct from "./components/products/edit-product";
import EditStorageUnit from "./components/storageunits/edit-storageunit";

class App extends Component {
  constructor(props) {
    super(props);
    this.logOut = this.logOut.bind(this);

    this.state = {
      showAdminBoard: false,
      currentUser: undefined,
    };
  }

  componentDidMount() {
    const user = AuthService.getCurrentUser();
    if (user) {
      this.setState({
        currentUser: user,
        showAdminMenu: user.role === "Admin"
      });
    }
  }

  logOut() {
    AuthService.logout();
  }

  render() {
    const { currentUser, showAdminMenu } = this.state;

    return (
      <div>
        <nav className="navbar navbar-expand navbar-dark bg-dark">
          <Link to={"/"} className="navbar-brand">
            Konku
          </Link>
          <div className="navbar-nav mr-auto">
            
            {showAdminMenu && (
              <li className="nav-item">
                <Link to={"/statistics"} className="nav-link">
                  Statistics
                </Link>
              </li>
            )}

            {currentUser && (
              <li className="nav-item">
                <Link to={"/storageunits"} className="nav-link">
                  Storage Units
                </Link>
              </li>
            )}

            {currentUser && (
              <li className="nav-item">
                <Link to={"/products"} className="nav-link">
                  Products
                </Link>
              </li>
            )}
          </div>

          {currentUser ? (
            <div className="navbar-nav ml-auto">
              <li className="nav-item">
                <Link to={"/"} className="nav-link">
                  {currentUser.username}
                </Link>
              </li>
              <li className="nav-item">
                <a href="/login" className="nav-link" onClick={this.logOut}>
                  Log out
                </a>
              </li>
            </div>
          ) : (
            <div className="navbar-nav ml-auto">
              <li className="nav-item">
                <Link to={"/login"} className="nav-link">
                  Login
                </Link>
              </li>

              <li className="nav-item">
                <Link to={"/register"} className="nav-link">
                  Register
                </Link>
              </li>
            </div>
          )}
        </nav>

        <div className="container mt-3">
          <Switch>
            <Route exact path="/" component={Profile} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/register" component={Register} />
            <Route exact path="/profile" component={Profile} />
            <AdminRoute exact path="/statistics" roles={[Role.Admin]} component={Statistics} />
            <Route exact path="/storageunits" component={StorageUnits} />
            <Route exact path="/add-storageunit" component={AddStorageUnit} />
            <Route exact path="/storageunits/:id" component={EditStorageUnit} />
            <Route exact path="/products" component={Products} />
            <Route exact path="/add-product" component={AddProduct} />
            <Route exact path="/products/:id" component={EditProduct} />
          </Switch>
        </div>
      </div>
    );
  }
}

export default App;
