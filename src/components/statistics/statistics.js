import React, { useState, useEffect } from "react";
import ProductService from "../../services/product.service";
import StorageUnitService from "../../services/storageunit.service";
import UserService from "../../services/user.service";


const Statistics = () => {
  const [products, setProducts] = useState([]);
  const [storageUnits, setStorageUnits] = useState([]);
  const [users, setUsers] = useState([]);

   useEffect(() => {
    retrieveProducts();
    retrieveStorageUnits();
    retrieveUsers();
  }, []);

  const retrieveProducts = () => {
    ProductService.getAll()
      .then(response => {
        setProducts(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  };

  const retrieveStorageUnits = () => {
    StorageUnitService.getAll()
      .then(response => {
        setStorageUnits(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  };

  const retrieveUsers = () => {
    UserService.getAll()
      .then(response => {
        setUsers(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  };

  return (

    <div className="container">
      <header className="jumbotron">
        <h3>
          <strong>Statistics</strong>
        </h3>
      </header>
      <p>
        <strong>Total admin accounts:</strong> {users.filter(({role}) => role === 'Admin').length}
      </p>
      <p>
        <strong>Total users accounts:</strong> {users.filter(({role}) => role === 'User').length}
      </p>
      <p>
        <strong>Total storage units:</strong> {storageUnits.length}
      </p>  
      <p>
        <strong>Total products:</strong> {products.length}
      </p>
    </div>

  );
};

export default Statistics;