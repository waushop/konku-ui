import React, { useState } from "react";
import { Link } from "react-router-dom";
import StorageUnitService from "../../services/storageunit.service";
import AuthService from "../../services/auth.service";

const AddStorageUnit = () => {
    const currentUser = AuthService.getCurrentUser();
    const initialStorageUnitState = {
      storageUnitId: null,
      name: "",
      userId: currentUser.id,
    };
    const [storageUnit, setStorageUnit] = useState(initialStorageUnitState);
    const [submitted, setSubmitted] = useState(false);
    
    const handleInputChange = event => {
      const { name, value } = event.target;
      setStorageUnit({ ...storageUnit, [name]: value });
    };

    const saveStorageUnit = () => {
      var data = {
        name: storageUnit.name,
        userId: storageUnit.userId
      };
  
      StorageUnitService.create(data)
        .then(response => {
          setStorageUnit({
            storageUnitId: response.data.storageUnitId,
            name: response.data.name,
            userId: response.data.userId
          });
          setSubmitted(true);
        })
        .catch(e => {
          console.log(e);
        });
    };
  
    const newStorageUnit = () => {
      setStorageUnit(initialStorageUnitState);
      setSubmitted(false);
    };
  
    return (
      <div className="submit-form">
        {submitted ? (
          <div>
          <h4 className="text-center mt-5">New storage unit added successfully!</h4>
          <button className="btn btn-success btn-block mt-5" onClick={newStorageUnit}>
            Add another storage unit
          </button>
          <Link to={"/storageunits"} className="nav-link btn btn-warning mt-2">Return to storage units list</Link>
        </div>
        ) : (
          <div>
            <div className="form-group">
              <label htmlFor="name">Name</label>
              <input
                type="text"
                className="form-control"
                id="name"
                required
                value={storageUnit.name}
                onChange={handleInputChange}
                name="name"
              />
            </div>
  
          <button onClick={saveStorageUnit} className="btn btn-success btn-block">
            Add new storage unit
          </button>
          <Link to={"/storageunits"} className="nav-link btn btn-warning mt-2">Return to storage units list</Link>
          </div>
        )}
      </div>
    );
  };
  
  export default AddStorageUnit;