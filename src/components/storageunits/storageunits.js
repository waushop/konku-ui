import React, { useState, useEffect, useMemo, useRef } from "react";
import StorageUnitService from "../../services/storageunit.service";
import { Link } from "react-router-dom";
import { useTable } from "react-table";

const StorageUnits = (props) => {
  const [storageUnits, setStorageUnits] = useState([]);
  const storageUnitsRef = useRef();

  storageUnitsRef.current = storageUnits;

  useEffect(() => {
    retrieveStorageUnits();
  }, []);

  const retrieveStorageUnits = () => {
    StorageUnitService.getUserStorageUnits()
      .then(response => {
        setStorageUnits(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  };

  const openStorageUnit = (rowIndex) => {
    const id = storageUnitsRef.current[rowIndex].storageUnitId;
    props.history.push("/storageunits/" + id);
  };

  const deleteStorageUnit = (rowIndex) => {
    const id = storageUnitsRef.current[rowIndex].storageUnitId;

    StorageUnitService.remove(id)
      .then((response) => {
        props.history.push("/storageunits");

        let newStorageUnits = [...storageUnitsRef.current];
        newStorageUnits.splice(rowIndex, 1);

        setStorageUnits(newStorageUnits);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const columns = useMemo(
    () => [
      {
        Header: "#",
        accessor: "storageUnitId",
      },
      {
        Header: "Name",
        accessor: "name",
      },
      {
        Header: "Actions",
        accessor: "actions",
        Cell: (props) => {
          const rowIdx = props.row.id;
          return (
            <div>
              <button className="btn btn-sm btn-warning mr-3" onClick={() => openStorageUnit(rowIdx)}>
                <i className="fas fa-pencil-alt"></i>
              </button>

              <button className="btn btn-sm btn-danger" onClick={() => deleteStorageUnit(rowIdx)}>
                <i className="fas fa-trash-alt"></i>
              </button>
            </div>
          );
        },
      },
    ],
    // eslint-disable-next-line
    []
  );

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
  } = useTable({
    columns,
    data: storageUnits,
  });

  return (
    <div className="container">
      <header className="jumbotron">
        <h3>
          <strong>Storage Units</strong>
        </h3>
      </header>
      <Link to={"/add-storageunit"} className="nav-link btn btn-primary mb-2">Add new storage unit</Link>
      <table
        className="table table-striped table-bordered"
        {...getTableProps()}
      >
        <thead>
          {headerGroups.map((headerGroup) => (
            <tr {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column) => (
                <th {...column.getHeaderProps()}>
                  {column.render("Header")}
                </th>
              ))}
            </tr>
          ))}
        </thead>
        <tbody {...getTableBodyProps()}>
          {rows.map((row, i) => {
            prepareRow(row);
            return (
              <tr {...row.getRowProps()}>
                {row.cells.map((cell) => {
                  return (
                    <td {...cell.getCellProps()}>{cell.render("Cell")}</td>
                  );
                })}
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};

export default StorageUnits;