import React, { useState, useEffect } from "react";
import StorageUnitService from "../../services/storageunit.service";

const EditStorageUnit = props => {
  const initialStorageUnitState = {
    storageUnitId: null,
    name: "",
  };
  const [currentStorageUnit, setCurrentStorageUnit] = useState(initialStorageUnitState);

  const getStorageUnit = id => {
    StorageUnitService.get(id)
      .then(response => {
        setCurrentStorageUnit(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  };
  useEffect(() => {
    getStorageUnit(props.match.params.id);
  }, [props.match.params.id]);

  const handleInputChange = event => {
    const { name, value } = event.target;
    setCurrentStorageUnit({ ...currentStorageUnit, [name]: value });
  };

  const updateStorageUnit = () => {
    StorageUnitService.update(currentStorageUnit.storageUnitId, currentStorageUnit)
      .then(response => {
        props.history.push("/storageunits");
      })
      .catch(e => {
        console.log(e);
      });
  };

  const deleteStorageUnit = () => {
    StorageUnitService.remove(currentStorageUnit.storageUnitId)
      .then(response => {
        props.history.push("/storageunits");
      })
      .catch(e => {
        console.log(e);
      });
  };

  return (
    <div>
        <div>
          <form>
            <div className="form-group">
              <label htmlFor="name">Name</label>
              <input
                type="text"
                className="form-control"
                id="name"
                name="name"
                value={currentStorageUnit.name}
                onChange={handleInputChange}
              />
            </div>
          </form>
          <button className="btn btn-block btn-success mr-2" onClick={updateStorageUnit}>
              Update product
            </button>
            <button
              type="submit" className="btn btn-block btn-danger" onClick={deleteStorageUnit}>
              Delete product
            </button>
        </div>
    </div>
  );
};

export default EditStorageUnit;