import React, { useState, useEffect } from "react";
import ProductService from "../../services/product.service";
import StorageUnitService from "../../services/storageunit.service";

const EditProduct = props => {
  const initialProductState = {
    productId: null,
    name: "",
    serialNumber: "",
    condition: "",
    quantity: "",
    storageUnitId: "",
  };
  const [currentProduct, setCurrentProduct] = useState(initialProductState);
  const [storageUnits, setStorageUnits] = useState([]);

  const getProduct = id => {
    ProductService.get(id)
      .then(response => {
        setCurrentProduct(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  };

  const retrieveStorageUnits = () => {
    StorageUnitService.getUserStorageUnits()
      .then(response => {
        setStorageUnits(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  };

  useEffect(() => {
    getProduct(props.match.params.id);
    retrieveStorageUnits();
  }, [props.match.params.id]);

  const handleInputChange = event => {
    const { name, value } = event.target;
    setCurrentProduct({ ...currentProduct, [name]: value });
  };

  const updateProduct = () => {
    ProductService.update(currentProduct.productId, currentProduct)
      .then(response => {
        props.history.push("/products");
      })
      .catch(e => {
        console.log(e);
      });
  };

  const deleteProduct = () => {
    ProductService.remove(currentProduct.productId)
      .then(response => {
        props.history.push("/products");
      })
      .catch(e => {
        console.log(e);
      });
  };

  return (
    <div>
      <div>
        <form>
          <div className="form-group">
            <label htmlFor="name">Name</label>
            <input
              type="text"
              className="form-control"
              id="name"
              name="name"
              value={currentProduct.name}
              onChange={handleInputChange}
            />
          </div>
          <div className="form-group">
            <label htmlFor="serialNumber">Serial number</label>
            <input
              type="text"
              className="form-control"
              id="serialNumber"
              name="serialNumber"
              value={currentProduct.serialNumber}
              onChange={handleInputChange}
            />
          </div>
          <div className="form-group">
            <label htmlFor="condition">Condition</label>
            <select className="form-control" id="condition" name="condition" onChange={handleInputChange} value={currentProduct.condition}>
              <option>Good</option>
              <option>Acceptable</option>
              <option>Trash</option>
            </select>
          </div>
          <div className="form-group">
            <label htmlFor="quantity">Quantity</label>
            <input
              type="number"
              className="form-control"
              id="quantity"
              name="quantity"
              value={currentProduct.quantity}
              onChange={handleInputChange}
            />
          </div>
          <div className="form-group">
            <label htmlFor="condition">Storage Unit</label>
            <select className="form-control" id="storageUnitId" name="storageUnitId" onChange={handleInputChange} value={currentProduct.storageUnitId}>
            {storageUnits.map(x =>
              <option key={x.storageUnitId} value={x.storageUnitId}>{x.name}</option>
            )};
            </select>
          </div>
        </form>
        <button className="btn btn-block btn-success mr-2" onClick={updateProduct}>
          Update product
            </button>
        <button
          type="submit" className="btn btn-block btn-danger" onClick={deleteProduct}>
          Delete product
            </button>
      </div>
    </div>
  );
};

export default EditProduct;