import React, { useState, useEffect, useMemo, useRef } from "react";
import ProductService from "../../services/product.service";
import { Link } from "react-router-dom";
import { useSortBy, useTable } from "react-table";
import { FaCaretSquareUp, FaCaretSquareDown } from "react-icons/fa";

const Products = (props) => {
  const [products, setProducts] = useState([]);
  const [pagingData, setPagingData] = useState([]);
  const productsRef = useRef();

  productsRef.current = products;

  useEffect(() => {
    retrieveProducts();
  }, []);

  const retrieveProducts = () => {
    ProductService.getUserProducts()
      .then(response => {
        setProducts(response.data);
        //TODO
        setPagingData(response.headers['pagination']);
      })
      .catch(e => {
        console.log(e);
      });
  };
  console.log(pagingData)

  const openProduct = (rowIndex) => {
    const id = productsRef.current[rowIndex].productId;
    props.history.push("/products/" + id);
  };

  const deleteProduct = (rowIndex) => {
    const id = productsRef.current[rowIndex].productId;

    ProductService.remove(id)
      .then((response) => {
        props.history.push("/products");

        let newProducts = [...productsRef.current];
        newProducts.splice(rowIndex, 1);

        setProducts(newProducts);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const columns = useMemo(
    () => [
      {
        Header: "#",
        accessor: "productId",
      },
      {
        Header: "Name",
        accessor: "name",
      },
      {
        Header: "Storage Unit",
        accessor: "storageUnitId",
      },
      {
        Header: "Actions",
        accessor: "actions",
        disableSortBy: true,
        Cell: (props) => {
          const rowIdx = props.row.id;
          return (
            <div>
              <button className="btn btn-sm btn-warning mr-3" onClick={() => openProduct(rowIdx)}>
                <i className="fas fa-pencil-alt"></i>
              </button>

              <button className="btn btn-sm btn-danger" onClick={() => deleteProduct(rowIdx)}>
                <i className="fas fa-trash-alt"></i>
              </button>
            </div>
          );
        },
      },
    ],
    // eslint-disable-next-line
    []
  );
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    rows,
  } = useTable(
    { columns, data: products },
    useSortBy,
    );

  return (
    <div className="container">
      <header className="jumbotron">
        <h3>
          <strong>Products</strong>
        </h3>
      </header>
      <Link to={"/add-product"} className="nav-link btn btn-primary mb-2">Add new product</Link>
      <table
        id="data-table"
        className="table table-striped table-bordered"
        {...getTableProps()}
      >
        <thead>
          {headerGroups.map((headerGroup) => (
            <tr {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column) => (
                <th {...column.getHeaderProps(column.getSortByToggleProps())}>
                  {column.render("Header")}
                  <span>
                    {column.isSorted ? (
                      column.isSortedDesc ? (
                        <FaCaretSquareDown />
                      ) : (
                        <FaCaretSquareUp />
                      )
                    ) : (
                      ""
                    )}
                  </span>
                </th>
              ))}
            </tr>
          ))}
        </thead>
        <tbody {...getTableBodyProps()}>
          {rows.map((row, i) => {
            prepareRow(row);
            return (
              <tr {...row.getRowProps()}>
                {row.cells.map((cell) => {
                  return (
                    <td {...cell.getCellProps()}>{cell.render("Cell")}</td>
                  );
                })}
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};

export default Products;