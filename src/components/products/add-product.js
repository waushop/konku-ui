import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import ProductService from "../../services/product.service";
import StorageUnitService from "../../services/storageunit.service";
import AuthService from "../../services/auth.service";

const AddProduct = () => {
  const currentUser = AuthService.getCurrentUser();
  const defaultImageSrc = '/img/default.png'
  const initialProductState = {
    productId: null,
    name: "",
    imageName: "",
    serialNumber: "",
    condition: "",
    quantity: null,
    imageSrc: defaultImageSrc,
    imageFile: null,
    storageUnitId: "",
    userId: currentUser.id,
  };
  const [product, setProduct] = useState(initialProductState);
  const [storageUnits, setStorageUnits] = useState([]);
  const [submitted, setSubmitted] = useState(false);
  const [errors, setErrors] = useState({})

  useEffect(() => {
    retrieveStorageUnits();
  }, []);

  const retrieveStorageUnits = () => {
    StorageUnitService.getUserStorageUnits()
      .then(response => {
        setStorageUnits(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  };

  const handleInputChange = event => {
    const { name, value } = event.target;
    setProduct({ ...product, [name]: value });
  };

  const showPreview = e => {
    if (e.target.files && e.target.files[0]) {
        let imageFile = e.target.files[0];
        const reader = new FileReader();
        reader.onload = x => {
            setProduct({
                ...product,
                imageFile,
                imageSrc: x.target.result
            })
        }
        reader.readAsDataURL(imageFile)
    }
    else {
        setProduct({
            ...product,
            imageFile: null,
            imageSrc: defaultImageSrc
        })
    }
  }

  const validate = () => {
    let temp = {}
    temp.name = product.name === "" ? false : true;
    temp.imageSrc = product.imageSrc === defaultImageSrc ? false : true;
    temp.storageUnitId = product.storageUnitId === "" ? false : true;
    setErrors(temp)
    return Object.values(temp).every(x => x === true)
  }
    
  const saveProduct = e => {
    e.preventDefault()
    if (validate()) {
    const data = new FormData()
    data.append('name', product.name)
    data.append('serialNumber', product.serialNumber)
    data.append('condition', product.condition)
    data.append('quantity', product.quantity)
    data.append('imageName', product.imageName)
    data.append('imageFile', product.imageFile)
    data.append('storageUnitId', product.storageUnitId)
    data.append('userId', product.userId)

    ProductService.create(data)
      .then(response => {
        setProduct({
          productId: response.data.productId,
          name: response.data.name,
          serialNumber: response.data.serialNumber,
          condition: response.data.condition,
          quantity: response.data.quantity,
          imageName: response.data.imageName,
          imageFile: response.data.imageFile,
          storageUnitId: response.data.storageUnitId,
          userId: response.data.userId
        });
        setSubmitted(true);
      })
      .catch(e => {
        console.log(e);
      });
    }
  };

  const newProduct = () => {
    setProduct(initialProductState);
    setSubmitted(false);
  };

  const applyErrorClass = field => ((field in errors && errors[field] === false) ? ' invalid-field' : '')

  return (
    <div className="submit-form">
      {submitted ? (
        <div>
          <h4 className="text-center mt-5">New product added successfully!</h4>
          <button className="btn btn-success btn-block mt-5" onClick={newProduct}>
            Add another product
          </button>
          <Link to={"/products"} className="nav-link btn btn-warning mt-2">Return to products list</Link>
        </div>
      ) : (
        
        <form autoComplete="off" noValidate onSubmit={saveProduct}>
          <div className="form-group">
            <label htmlFor="name">Name</label>
            <input
              type="text"
              className={"form-control" + applyErrorClass('name')}
              id="name"
              required
              value={product.name}
              onChange={handleInputChange}
              name="name"
            />
          </div>

          <div className="form-group">
            <label htmlFor="name">Serial number</label>
            <input
              type="text"
              className="form-control"
              id="serialNumber"
              required
              value={product.serialNumber}
              onChange={handleInputChange}
              name="serialNumber"
            />
          </div>

          <div className="form-group">
            <label htmlFor="condition">Condition</label>
            <select className="form-control" id="condition" name="condition" onChange={handleInputChange} value={product.condition}>
              <option>Good</option>
              <option>Acceptable</option>
              <option>Trash</option>
            </select>
          </div>

          <div className="form-group">
            <label htmlFor="name">Quantity</label>
            <input
              type="number"
              className="form-control"
              id="quantity"
              required
              value={product.quantity}
              onChange={handleInputChange}
              name="quantity"
            />
          </div>

          <div className="form-group">
            <label htmlFor="storageUnitId">Image</label>
            <img src={product.imageSrc} alt={product.imageName} className="" width="200px" />
            <input
              type="file" 
              accept="image/*" 
              className={"form-control-file" + applyErrorClass('imageSrc')}
              onChange={showPreview} 
              id="image" />
          </div>

          <div className="form-group">
            <label htmlFor="condition">Storage Unit</label>
            <select className={"form-control" + applyErrorClass('storageUnitId')} id="storageUnitId" name="storageUnitId" onChange={handleInputChange} value={product.storageUnitId}>
            {storageUnits.map(x =>
              <option key={x.storageUnitId} value={x.storageUnitId}>{x.name}</option>
            )};
            </select>
          </div>

          <div className="form-group">
            <input
              type="hidden"
              className="form-control"
              id="storageUnitId"
              required
              value={currentUser.id}
              onChange={handleInputChange}
              name="userId"
            />
          </div>

          <button type="submit" className="btn btn-success btn-block">
            Add new product
          </button>
          <Link to={"/products"} className="nav-link btn btn-warning mt-2">Return to products list</Link>
        </form>
      )}
    </div>
  );
};

export default AddProduct;